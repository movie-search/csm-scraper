# csm-scraper

    const csm = require('csm')
    csm('Shrek')
      .then(result => {
        /*
          {
            "age": "age 6+",
            "parentsSay": "age 7+",
            "kidsSay": "age 7+",
            "oneLiner": "Gross-out laughs meet a marvelous fairy tale mix.",
            "parentsNeedToKnow": "Parents need to know that Shrek includes some edgy humor directed at teens and adults. The jokes that teens and adults snicker at (like when Shrek wonders whether the small Lord Farquaad is compensating for something with his very tall castle) will be over the head of most younger kids, but parents should be ready for some questions. There's also plenty of potty humor and gross-out jokes directed at kids – mostlybased on the appalling personal habits of ogres. Scary scenes for young ones include fights with guards, villagers coming after Shrek with pitchforks, and a fire-breathing dragon (who turns nice when she falls for Donkey). A bird explodes, and its eggs are eaten, and a character is eaten in one gulp by the dragon, but it's not graphic.\n",
            "inThisMovie": {
              "message": {
                "rating": 4,
                "text": "Staying true to yourself, looking beyond appearances, and the power of friendship are big themes. (But there's also a fair bit of bodily function humor.)\n"
              },
              "violence": {
                "rating": 3,
                "text": "Characters in peril; ogre hunters wave pitchforks and torches; a bird explodes; scary fire-breathing dragon (who is much less scarywhen she gets a crush on Donkey); one character is eaten in one gulp by the dragon.\n"
              },
              "model": {
                "rating": 4,
                "text": "Princess Fiona is a strong character who challenges the prim 'n' proper princess stereotype. Shrek seems cantankerous and rude, buthe has a tender heart and is ultimately trustworthy, loyal, and brave. Donkey is a dedicated friend. Many characters demonstrate the qualities of curiosity, integrity, perseverance, and teamwork.\n"
              },
              "sex": {
                "rating": 1,
                "text": "Mild sexual humor. Some innuendo that will go over kids' head (for example, when Shrek sees the big tower that is Farquaad's castleand says to Donkey, \"Gee, think he's compensating for something?\").\n"
              },
              "language": {
                "rating": 3,
                "text": "Strong language for a kids' movie, including \"damn,\" \"ass,\" and \"crap.\"\n"
              },
              "consumerism": {
                "rating": 2,
                "text": "Lots of tie-in products available in real life.\n"
              },
              "drugs": {
                "rating": 0,
                "text": ""
              }
            },
            "familyTopics": "Families can talk about Donkey's statement that Shrek has \"that kind of 'I don't care what nobody thinks of me' thing.\" Is ittrue that Shrek didn't care what people thought of him? How can you tell? What did it mean to say that ogres are like onions? What does it mean tosay that people have layers?\n\nPrincess Fiona expected Prince Charming to save her, and Shrek came instead. How did she change her mind about him? How did it help her to accept herself? Why is self-acceptance so important?\n\nHow do the characters in Shrek demonstrate perseverance and teamwork? What about curiosity and integrity? Why are these important character strengths?\n\n"
          }
        */
      })