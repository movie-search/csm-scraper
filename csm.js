const rp = require('request-promise-native'),
      cheerio = require('cheerio'),
      fs = require('fs')

const url = 'https://www.commonsensemedia.org'

transformTitle = title => title.replace(/[^a-z0-9+]+/gi, '-').toLowerCase()
determineRating = cheerioObj => {
  const objClass = cheerioObj.attr('class')
  for(let i=0;i<6;i++){
    const rating = `content-grid-${i}`
    if(objClass.indexOf(rating) >= 0){
      return i
    }
  }
}

fetchHtml = title => new Promise ((resolve, reject) => {
  title = transformTitle(title)
  rp(`${url}/movie-reviews/${title}`)
    .then(htmlResult => {
      resolve(extractData(htmlResult))
    })
    .catch(err => reject(err))
})

extractData = htmlResult => {
  let $ = cheerio.load(htmlResult)
  return result = {
    age: $('.field-name-field-review-recommended-age .csm-green-age').text(),
    parentsSay: $('.user-review-statistics.adult .age').text(),
    kidsSay:$('.user-review-statistics.child .age').text(),
    oneLiner: $('.field-name-field-one-liner').text(),
    parentsNeedToKnow: $('.field-name-field-parents-need-to-know').text(),
    inThisMovie:{
      message:{
        rating:determineRating($('#content-grid-item-message .field_content_grid_rating')),
        text:$('#content-grid-item-message .field-name-field-content-grid-rating-text').text()
      },
      violence:{
        rating:determineRating($('#content-grid-item-violence .field_content_grid_rating')),
        text:$('#content-grid-item-violence .field-name-field-content-grid-rating-text').text()
      },
      model:{
        rating:determineRating($('#content-grid-item-role_model .field_content_grid_rating')),
        text:$('#content-grid-item-role_model .field-name-field-content-grid-rating-text').text()
      },
      sex:{
        rating:determineRating($('#content-grid-item-sex .field_content_grid_rating')),
        text:$('#content-grid-item-sex .field-name-field-content-grid-rating-text').text()
      },
      language:{
        rating:determineRating($('#content-grid-item-language .field_content_grid_rating')),
        text:$('#content-grid-item-language .field-name-field-content-grid-rating-text').text()
      },
      consumerism:{
        rating:determineRating($('#content-grid-item-consumerism .field_content_grid_rating')),
        text:$('#content-grid-item-consumerism .field-name-field-content-grid-rating-text').text()
      },
      drugs:{
        rating:determineRating($('#content-grid-item-drugs .field_content_grid_rating')),
        text:$('#content-grid-item-drugs .field-name-field-content-grid-rating-text').text()
      }
    },
    familyTopics:$('.field-name-field-family-topics').text(),
  }
}

module.exports = fetchHtml
